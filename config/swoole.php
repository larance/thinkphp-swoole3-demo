<?php

//use think\swoole\websocket\socketio\Handler;
//use think\swoole\websocket\socketio\Parser;
use app\websocket\privater\Handler;
use app\websocket\privater\Parser;

use app\subscribe\WebSocketEvent;
use \Swoole\Table;

return [
    'server' => [
        'host' => env('SWOOLE_HOST', '0.0.0.0'), // 监听地址
        'port' => env('SWOOLE_PORT', 9501), // 监听端口
        'mode' => SWOOLE_PROCESS, // 运行模式 默认为SWOOLE_PROCESS
        'sock_type' => SWOOLE_SOCK_TCP | SWOOLE_SSL, // sock type 默认为SWOOLE_SOCK_TCP
        'options' => [
            'pid_file' => runtime_path() . 'swoole.pid',
            'log_file' => runtime_path() . 'swoole.log',
            'daemonize' => false,
            // Normally this value should be 1~4 times larger according to your cpu cores.
            'reactor_num' => swoole_cpu_num(),
            'worker_num' => swoole_cpu_num(),
            'task_worker_num' => swoole_cpu_num(),
            'enable_static_handler' => true,
            'document_root' => root_path('public'),
            'package_max_length' => 20 * 1024 * 1024,
            'buffer_output_size' => 10 * 1024 * 1024,
            'socket_buffer_size' => 128 * 1024 * 1024,
	        'max_request' => 1000,
            'task_enable_coroutine' => true,
            'send_yield' => true,
            'reload_async' => true,
            'enable_coroutine' => true,
            'task_max_request' => 1000,
//            'ssl_cert_file' => '',
//            'ssl_key_file' => '',
        ],
    ],
    'websocket' => [
        'enable' => true,
        'handler' => Handler::class,
        'parser' => Parser::class,
        'ping_interval' => 25000,
        'ping_timeout' => 60000,
        'room' => [
            'type' => 'table',
            'table' => [
                'room_rows' => 4096,
                'room_size' => 2048,
                'client_rows' => 8192,
                'client_size' => 2048,
            ],
            'redis' => [
                'host' => '127.0.0.1',
                'port' => 6379,
                'max_active' => 3,
                'max_wait_time' => 5,
            ],
        ],
        'listen' => [],
        'subscribe' => [
            WebSocketEvent::class
        ],
    ],
    'rpc' => [
        'server' => [
            'enable' => false,
            'port' => 9000,
            'services' => [
            ],
        ],
        'client' => [
        ],
    ],
    'hot_update' => [
//        'enable'  => env('APP_DEBUG', false),
        'enable' => true,
        'name' => ['*.php', '.env'],
        'include' => [root_path()],
        'exclude' => [],
    ],
    //连接池
    'pool' => [
        'db' => [
            'enable' => true,
            'max_active' => 3,
            'max_wait_time' => 5,
        ],
        'cache' => [
            'enable' => true,
            'max_active' => 3,
            'max_wait_time' => 5,
        ],
        //自定义连接池
    ],
    'coroutine' => [
        'enable' => true,
        'flags' => SWOOLE_HOOK_ALL,
    ],
    'tables' => [
        'u2fd' => [
            'size' => 10240,
            'columns' => [
                ['name' => 'fd', 'type' => Table::TYPE_INT, 'size' => 8]
            ]
        ],
        'fd2u' => [
            'size' => 10240,
            'columns' => [
                ['name' => 'uid', 'type' => Table::TYPE_INT, 'size' => 8]
            ]
        ],
        'vote' => [
            'size' => 256,
            'columns' => [
                ['name' => 'poll', 'type' => Table::TYPE_INT, 'size' => 8],//票数
//                ['name' => 'icon', 'type' => Table::TYPE_STRING, 'size' => 128]//头像
            ]
        ],
    ],
    //每个worker里需要预加载以共用的实例
    'concretes' => [],
    //重置器
    'resetters' => [],
    //每次请求前需要清空的实例
    'instances' => [],
    //每次请求前需要重新执行的服务
    'services' => [],
];
